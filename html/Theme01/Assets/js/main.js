﻿$(".course__slider").slick({
	//autoplay: true,
	arrows: false,
	dots: true,
	centerMode: true,
	centerPadding: '0',
	slidesToShow: 3,
	responsive: [{

		breakpoint: 1024,
		settings: {
			slidesToShow: 2,
			centerMode: false
		}

	}, {

		breakpoint: 600,
		settings: {
			slidesToShow: 1,
			centerMode: false
		}

	}]
});

$(".coach__slider").slick({
	//autoplay: true,
	arrows: false,
	dots: true
});